lexer grammar LLLexer;

fragment DIGIT: [0-9];
fragment LETTER : [a-zA-Z\u0080-\uFFFF];

NUM_INT: DIGIT+;
NUM_FLOAT: DIGIT+ (DOT DIGIT*)?;

STRING_VALUE: '"' .*? '"';

FUNC: 'func';
RETURN: 'return';
IF: 'if';
ELSE: 'else';
FOR: 'for';
WHILE: 'while';

WHITESPACE: ' ' -> skip;
NEWLINE: '\r'? '\n' -> skip;
TABULATOR: '\t' -> skip;

VOID_TYPE: 'void';
STRING_TYPE: 'string';
UINT8: 'u8';
UINT16: 'u16';
UINT32: 'u32';
UINT64: 'u64';
INT8: 'i8';
INT16: 'i16';
INT32: 'i32';
INT64: 'i64';
FLOAT32: 'f32';
FLOAT64: 'f64';
BOOL_TYPE: 'bool';
TRUE: 'true';
FALSE: 'false';

ADD: '+';
SUB: '-';
MUL: '*';
DIV: '/';
LESS_THAN: '<';
GREATER_THAN: '>';
LESS_EQ_THAN: '<=';
GREATER_EQ_THAN: '>=';
NOT_EQUAL: '!=';
EQUAL: '==';
NOT: '!';
AND: '&&';
OR: '||';
XOR: '^';
FUNC_RET: '->';

EQUALS: '=';
LPAR: '(';
RPAR: ')';
LBRACE: '{';
RBRACE: '}';
LBRACKET: '[';
RBRACKET: ']';
DOT: '.';
COMMA: ',';
COLON: ':';
SEMI_COLON: ';';
QUOTES: '"';

COMMENT_SINGLE: '//' ~[\r\n]* '\r'? '\n' -> skip;
COMMENT_MULTIPLE: '/*' .*? '*/' -> skip;

ID: LETTER (LETTER | DIGIT)*;
