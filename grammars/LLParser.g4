parser grammar LLParser;
options {
	tokenVocab=LLLexer;
}

type_builtin: UINT8 | UINT16 | UINT32 | UINT64 | INT8 | INT16 | INT32 | INT64 | FLOAT32 | FLOAT64 | BOOL_TYPE | STRING_TYPE | VOID_TYPE;


type: type_builtin | ID;
return_type: FUNC_RET type_builtin;
bool_value: TRUE | FALSE;
number_value: NUM_INT | NUM_FLOAT;
value: number_value | STRING_VALUE | ID | bool_value;

program: function+ EOF;
function: FUNC ID LPAR param_list RPAR return_type statement_block;
statement_block: LBRACE statement* RBRACE;
statement: control_stmt | (singular_stmt SEMI_COLON);

control_stmt: if_stmt | for_stmt | while_stmt;
singular_stmt: return_stmt | decl_stmt | expr_stmt | call_stmt;

return_stmt: RETURN value?;
decl_stmt: decl EQUALS value;
expr_stmt: ID EQUALS expr;
call_stmt: ID LPAR value* RPAR;

if_stmt: IF LPAR (expr | value) RPAR statement_block (ELSE statement_block)?;
for_stmt: FOR LPAR decl_stmt? SEMI_COLON expr? SEMI_COLON expr_stmt? RPAR statement_block;
while_stmt: WHILE LPAR (expr | value) RPAR statement_block;

expr: binary_expr | unary_expr;
binary_expr: value binary_op value;
unary_expr: unary_op value;
binary_op: ADD | SUB | MUL | DIV | LESS_THAN | GREATER_THAN | LESS_EQ_THAN | GREATER_EQ_THAN | EQUAL | NOT_EQUAL | AND | OR | XOR;
unary_op: NOT | SUB;
decl: type COLON ID;
param_list: (decl (COMMA decl)*)?;
