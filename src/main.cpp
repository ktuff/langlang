#include <iostream>
#include <ANTLRFileStream.h>
#include "LLLexer.h"
#include "LLParser.h"

#include "visitor.hpp"

int main(int argc, char **argv)
{
	if (argc < 3) {
		std::cout << argv[0] << " <input-file> <output-file>" << std::endl;
		return 0;
	}

	// parsing
	antlr4::ANTLRFileStream stream(argv[1]);
	ll::LLLexer lexer(&stream);
	antlr4::CommonTokenStream tokens(&lexer);
	ll::LLParser parser(&tokens);
	ll::LLParser::ProgramContext *prog = parser.program();

	// codegen
	LangVisitor visitor;
	visitor.visitProgram(prog);
	visitor.emit_object(argv[2]);

	return 0;
}
