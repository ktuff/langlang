#include <system_error>
#include "LLLexer.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/LegacyPassManager.h"
#include <llvm/IR/Value.h>
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include "visitor.hpp"


LangVisitor::LangVisitor()
: m_current_bb(nullptr)
{
	this->m_context = std::make_unique<llvm::LLVMContext>();
	this->m_module = std::make_unique<llvm::Module>("langlang module", *m_context);
	this->m_builder = std::make_unique<llvm::IRBuilder<>>(*m_context);
}

LangVisitor::~LangVisitor()
{
	//this->m_module->print(llvm::errs(), nullptr);
}


antlrcpp::Any LangVisitor::visitProgram(ll::LLParser::ProgramContext *ctx)
{
	return visitChildren(ctx);
}

antlrcpp::Any LangVisitor::visitFunction(ll::LLParser::FunctionContext *ctx)
{
	this->m_current_bb = nullptr;
	auto func_name = ctx->ID()->getText();
	if (m_functions.find(func_name) == m_functions.end()) {
		this->m_functions.emplace(func_name, Function());
	} else {
		throw std::runtime_error("Redeclaration of function");
	}

	// function return type
    llvm::Type* ret_type = get_type(ctx->return_type()->type_builtin());
	
	// function arguments
	std::vector<llvm::Type*> args;
	auto params = visitParam_list(ctx->param_list());

	// create function
	llvm::FunctionType *func_type = llvm::FunctionType::get(ret_type, args, false);
	llvm::Function *func = llvm::Function::Create(func_type, llvm::Function::ExternalLinkage, func_name, m_module.get());

	// Create a new basic blocks to start insertion into.
	llvm::BasicBlock *bb_begin = llvm::BasicBlock::Create(*m_context, "", func);
	this->m_return_value = new llvm::AllocaInst(ret_type, 0, "return_value", bb_begin);

	this->m_current_bb = bb_begin;
	this->m_builder->SetInsertPoint(m_current_bb);
	this->visitStatement_block(ctx->statement_block());

	llvm::PointerType* p_type = llvm::dyn_cast<llvm::PointerType>(m_return_value->getType());
	llvm::LoadInst* loaded_ret = m_builder->CreateLoad(p_type->getElementType(), m_return_value);
	this->m_builder->CreateRet(loaded_ret);

	this->m_current_bb = nullptr;

	// if error reading body, remove function.
	// func->eraseFromParent();

	return func;
}

antlrcpp::Any LangVisitor::visitStatement_block(ll::LLParser::Statement_blockContext * ctx)
{
	for (auto* st_ctx : ctx->statement()) {
		this->visitStatement(st_ctx);
	}
	return nullptr;
}

antlrcpp::Any LangVisitor::visitReturn_stmt(ll::LLParser::Return_stmtContext *ctx)
{
	llvm::Value* value = visitValue(ctx->value());
	if (llvm::isa<llvm::PointerType>(value->getType())) {
		llvm::PointerType* p_type = llvm::dyn_cast<llvm::PointerType>(value->getType());
		value = m_builder->CreateLoad(p_type->getElementType(), value);
	}
	this->store_value(value, m_return_value);
	return nullptr;
}

antlrcpp::Any LangVisitor::visitDecl_stmt(ll::LLParser::Decl_stmtContext *ctx)
{
	auto* decl = ctx->decl();
	auto* type = decl->type();
	std::string id = decl->ID()->getText();
	llvm::Type* llvm_type = nullptr;
	if (type->type_builtin()) {
		llvm_type = get_type(type->type_builtin());
	} else if (type->ID()) {
		throw std::runtime_error("not implemented");
	}
	if (m_local_variables.find(id) == m_local_variables.end()) {
		llvm::AllocaInst* alloca_inst = new llvm::AllocaInst(llvm_type, 0, id, m_current_bb);
		this->m_local_variables.emplace(id, alloca_inst);
		llvm::Value* value = to_element(visitValue(ctx->value()).as<llvm::Value*>());
		this->store_value(value, alloca_inst);
	} else {
		throw std::runtime_error("duplicated local variable");
	}
    return nullptr;
}

antlrcpp::Any LangVisitor::visitExpr_stmt(ll::LLParser::Expr_stmtContext *ctx)
{
	std::string var_name = ctx->ID()->getText();
	if (m_local_variables.find(var_name) == m_local_variables.end()) {
		throw std::runtime_error("unknown variable name");
	}
	llvm::Value* var = m_local_variables.at(var_name);
	auto e = visitExpr(ctx->expr());
	llvm::Value* expr = e.as<llvm::Value*>();
	this->store_value(expr, var);
    return nullptr;
}

antlrcpp::Any LangVisitor::visitIf_stmt(ll::LLParser::If_stmtContext *ctx)
{
	llvm::BasicBlock* bb = m_current_bb;
	
	llvm::Value* cond = nullptr;
	if (auto* e = ctx->expr()) {
		cond = visitExpr(e).as<llvm::Value*>();
		if (!llvm::isa<llvm::ICmpInst>(cond)) {
			throw std::runtime_error("Invalid expression in if statement");
		}
	} else if (auto* v = ctx->value()) {
		llvm::Value* value = visitValue(v).as<llvm::Value*>();
		if (!llvm::isa<llvm::Constant>(value)) {
			value = to_element(value);
		}
		llvm::Value* null_value = llvm::Constant::getNullValue(value->getType());
		cond = new llvm::ICmpInst(*m_current_bb, llvm::CmpInst::Predicate::ICMP_NE, value, null_value, "");
	} else {
		throw std::runtime_error("Invalid if-statement");
	}

	llvm::Function* func = bb->getParent();
	llvm::BasicBlock *bb_then = llvm::BasicBlock::Create(*m_context, "then", func);
	llvm::BasicBlock *bb_else = llvm::BasicBlock::Create(*m_context, "else", func);
	llvm::BasicBlock *bb_after = llvm::BasicBlock::Create(*m_context, "", func);
	this->m_builder->CreateCondBr(cond, bb_then, bb_else);

	// then
	this->m_current_bb = bb_then;
	this->m_builder->SetInsertPoint(m_current_bb);
	this->visitStatement_block(ctx->statement_block(0));
	this->m_builder->CreateBr(bb_after);
	// else
	this->m_current_bb = bb_else;
	this->m_builder->SetInsertPoint(m_current_bb);
	this->visitStatement_block(ctx->statement_block(1));
	this->m_builder->CreateBr(bb_after);

	this->m_current_bb = bb_after;
	this->m_builder->SetInsertPoint(m_current_bb);
	
	return nullptr;
}

antlrcpp::Any LangVisitor::visitFor_stmt(ll::LLParser::For_stmtContext *ctx)
{
	if (ctx->decl_stmt()) {
		this->visitDecl_stmt(ctx->decl_stmt());
	}

	llvm::BasicBlock* bb = m_current_bb;
	llvm::Function* func = bb->getParent();

	llvm::BasicBlock *bb_cond = llvm::BasicBlock::Create(*m_context, "cond", func);
	llvm::BasicBlock *bb_then = llvm::BasicBlock::Create(*m_context, "then", func);
	llvm::BasicBlock *bb_after = llvm::BasicBlock::Create(*m_context, "", func);
	this->m_builder->CreateBr(bb_cond);

	this->m_current_bb = bb_cond;
	this->m_builder->SetInsertPoint(m_current_bb);
	llvm::Value* value = nullptr;
	if (auto* e = ctx->expr()) {
		auto v = visitExpr(e);
		value = v.as<llvm::Value*>();
	} else {
		value = m_builder->getTrue();
	}
	this->m_builder->CreateCondBr(value, bb_then, bb_after);

	this->m_current_bb = bb_then;
	this->m_builder->SetInsertPoint(m_current_bb);
	this->visitStatement_block(ctx->statement_block());
	if (ctx->expr_stmt()) {
		this->visitExpr_stmt(ctx->expr_stmt());
	}
	this->m_builder->CreateBr(bb_cond);
	this->m_current_bb = bb_after;
	this->m_builder->SetInsertPoint(m_current_bb);

	return nullptr;
}

antlrcpp::Any LangVisitor::visitWhile_stmt(ll::LLParser::While_stmtContext *ctx)
{
	llvm::BasicBlock *bb = m_current_bb;

	llvm::Function* func = m_current_bb->getParent();
	llvm::BasicBlock *bb_cond = llvm::BasicBlock::Create(*m_context, "cond", func);
	llvm::BasicBlock *bb_then = llvm::BasicBlock::Create(*m_context, "then", func);
	llvm::BasicBlock *bb_after = llvm::BasicBlock::Create(*m_context, "", func);
	this->m_builder->CreateBr(bb_cond);

	this->m_current_bb = bb_cond;
	this->m_builder->SetInsertPoint(m_current_bb);
	llvm::Value* cond = nullptr;
	if (auto* e = ctx->expr()) {
		auto v = visitExpr(e);
		cond = v.as<llvm::Value*>();
	} else if (auto* v = ctx->value()) {
		auto v1 = visitValue(v);
		llvm::Value* val = v1.as<llvm::Value*>();
		if (!llvm::dyn_cast<llvm::AllocaInst>(val)) {
			llvm::AllocaInst* alloca_inst = m_builder->CreateAlloca(val->getType());
			this->store_value(val, alloca_inst);
			val = alloca_inst;
		}
		llvm::PointerType* p_type = llvm::dyn_cast<llvm::PointerType>(val->getType());
		llvm::LoadInst* load_inst = m_builder->CreateLoad(p_type->getElementType(), val);
		llvm::Value* null_value = llvm::Constant::getNullValue(load_inst->getType());
		cond = new llvm::ICmpInst(*m_current_bb, llvm::CmpInst::Predicate::ICMP_NE, load_inst, null_value, "");
	} else {
		throw std::runtime_error("Invalid if-statement");
	}
	this->m_builder->CreateCondBr(cond, bb_then, bb_after);

	this->m_current_bb = bb_then;
	this->m_builder->SetInsertPoint(m_current_bb);
	this->visitStatement_block(ctx->statement_block());
	llvm::BranchInst::Create(bb_cond, bb_then);
	this->m_current_bb = bb_after;
	this->m_builder->SetInsertPoint(m_current_bb);

	return nullptr;
}

antlrcpp::Any LangVisitor::visitBinary_expr(ll::LLParser::Binary_exprContext *ctx)
{
	llvm::Value* value1 = to_element(visitValue(ctx->value(0)).as<llvm::Value*>());
	llvm::Value* value2 = to_element(visitValue(ctx->value(1)).as<llvm::Value*>());
	if (value1->getType() != value2->getType()) {
		//throw std::runtime_error("expression has different values");
	}
	llvm::Type* t1 = value1->getType();
	llvm::Type* t2 = value2->getType();
	if (llvm::dyn_cast<llvm::IntegerType>(t1)->getBitWidth() > llvm::dyn_cast<llvm::IntegerType>(t2)->getBitWidth()) {
		value2 = m_builder->CreateSExt(value2, t1);
	} else if (llvm::dyn_cast<llvm::IntegerType>(t1)->getBitWidth() < llvm::dyn_cast<llvm::IntegerType>(t2)->getBitWidth()) {
		value1 = m_builder->CreateSExt(value1, t2);
	}
	size_t op_type = ctx->binary_op()->start->getType();
	switch (op_type) {
	case ll::LLLexer::ADD:
		return m_builder->CreateAdd(value1, value2);
	case ll::LLLexer::SUB:
		return m_builder->CreateSub(value1, value2);
	case ll::LLLexer::MUL:
		return m_builder->CreateMul(value1, value2);
	case ll::LLLexer::DIV:
		return m_builder->CreateSDiv(value1, value2);
	case ll::LLLexer::LESS_THAN:
		return m_builder->CreateICmpSLT(value1, value2);
	case ll::LLLexer::GREATER_THAN:
		return m_builder->CreateICmpSGT(value1, value2);
	case ll::LLLexer::LESS_EQ_THAN:
		return m_builder->CreateICmpSLE(value1, value2);
	case ll::LLLexer::GREATER_EQ_THAN:
		return m_builder->CreateICmpSGE(value1, value2);
	case ll::LLLexer::EQUAL:
		return m_builder->CreateICmpEQ(value1, value2);
	case ll::LLLexer::NOT_EQUAL:
		return m_builder->CreateICmpNE(value1, value2);
	case ll::LLLexer::AND:
		return m_builder->CreateAnd(value1, value2);
	case ll::LLLexer::OR:
		return m_builder->CreateOr(value1, value2);
	case ll::LLLexer::XOR:
		return m_builder->CreateXor(value1, value2);
	}
	return nullptr;
}

antlrcpp::Any LangVisitor::visitUnary_expr(ll::LLParser::Unary_exprContext *ctx)
{
	auto v1 = visitValue(ctx->value());
	llvm::Value* value1 = v1.as<llvm::Value*>();
	size_t op_type = ctx->unary_op()->start->getType();
	switch (op_type) {
	case ll::LLLexer::NOT:
		return m_builder->CreateNot(value1);
	case ll::LLLexer::SUB:
		return m_builder->CreateNeg(value1);
	}
		
	return nullptr;
}

antlrcpp::Any LangVisitor::visitValue(ll::LLParser::ValueContext *ctx)
{
	if (auto* bv = ctx->bool_value()) {
		if (bv->TRUE()) {
			return m_builder->getTrue();
		} else if (bv->FALSE()) {
			return m_builder->getFalse();
		}
	} else if (auto* nv = ctx->number_value()) {
		if (nv->NUM_INT()) {
			return m_builder->getInt64(std::stoi(nv->getText()));
		} else if (nv->NUM_FLOAT()) {
			return llvm::ConstantFP::get(m_builder->getDoubleTy(), nv->getText());
		}
	} else if (ctx->STRING_VALUE()) {
		return nullptr;
	} else if (ctx->ID()) {
		std::string id = ctx->ID()->getText();
		if (m_local_variables.find(id) != m_local_variables.end()) {
			return m_local_variables.at(id);
		} else {
			throw std::runtime_error("unknown variable name");
		}
	} else {
		throw std::runtime_error("Invalid value");
	}
	return nullptr;
}

void LangVisitor::emit_object(const std::string& output)
{
	// target
	auto target_triple = llvm::sys::getDefaultTargetTriple();
	llvm::InitializeAllTargetInfos();
	llvm::InitializeAllTargets();
	llvm::InitializeAllTargetMCs();
	llvm::InitializeAllAsmParsers();
	llvm::InitializeAllAsmPrinters();

	std::string err;
	auto target = llvm::TargetRegistry::lookupTarget(target_triple, err);
	
	// target machine
	auto cpu = "generic";
	auto features = "";
	
	llvm::TargetOptions opt;
	auto rm = llvm::Optional<llvm::Reloc::Model>();
	auto target_machine = target->createTargetMachine(target_triple, cpu, features, opt, rm);
	
	// module configuration
	this->m_module->setDataLayout(target_machine->createDataLayout());
	this->m_module->setTargetTriple(target_triple);
	
	// emit object code
	auto filename = "output.o";
	std::error_code ec;
	llvm::raw_fd_ostream dest(filename, ec, llvm::sys::fs::OF_None);
	if (ec) {
		llvm::errs() << "Could not open file: " << ec.message();
		return;
	}
	
	llvm::legacy::PassManager pass;
	auto file_type = llvm::CGFT_ObjectFile;
	
	if (target_machine->addPassesToEmitFile(pass, dest, nullptr, file_type)) {
		llvm::errs() << "TargetMachine can't emit a file of this type";
		return;
	}
	
	pass.run(*m_module);
	dest.flush();
}


llvm::Type* LangVisitor::get_type(ll::LLParser::Type_builtinContext* builtin) const
{
	size_t ll_type = builtin->start->getType();
	switch (ll_type) {
	case ll::LLLexer::INT8:
		return m_builder->getInt8Ty();
	case ll::LLLexer::INT16:
		return m_builder->getInt16Ty();
	case ll::LLLexer::INT32:
		return m_builder->getInt32Ty();
	case ll::LLLexer::INT64:
		return m_builder->getInt64Ty();
	case ll::LLLexer::UINT8:
		return m_builder->getInt8Ty();
	case ll::LLLexer::UINT16:
		return m_builder->getInt16Ty();
	case ll::LLLexer::UINT32:
		return m_builder->getInt32Ty();
	case ll::LLLexer::UINT64:
		return m_builder->getInt64Ty();
	case ll::LLLexer::FLOAT32:
		return m_builder->getFloatTy();
	case ll::LLLexer::FLOAT64:
		return m_builder->getDoubleTy();
	case ll::LLLexer::VOID_TYPE:
		return m_builder->getVoidTy();
	case ll::LLLexer::BOOL_TYPE:
		return m_builder->getInt1Ty();
	case ll::LLLexer::STRING_TYPE:
		throw std::runtime_error("string not implemented");
	default:
		throw std::runtime_error("unknown type: " + std::to_string(ll_type));
	}
}

llvm::Value* LangVisitor::to_element(llvm::Value* value)
{
	if (!llvm::isa<llvm::Constant>(value)) {
		llvm::PointerType* p_type = llvm::dyn_cast<llvm::PointerType>(value->getType());
		return m_builder->CreateLoad(p_type->getElementType(), value);
	} else {
		return value;
	}
}

void LangVisitor::store_value(llvm::Value* value, llvm::Value* target)
{
	llvm::Type* t = value->getType();
	llvm::Type* type = target->getType();
	if (llvm::isa<llvm::PointerType>(type)) {
		llvm::PointerType* p_type = llvm::dyn_cast<llvm::PointerType>(type);
		type = p_type->getElementType();
	}
	if (!llvm::isa<llvm::IntegerType>(t) || !llvm::isa<llvm::IntegerType>(type)) {
		this->m_builder->CreateStore(value, target);
		return;
	}

	llvm::IntegerType* it = llvm::dyn_cast<llvm::IntegerType>(t);
	llvm::IntegerType* itype = llvm::dyn_cast<llvm::IntegerType>(type);
	if (it->getBitWidth() > itype->getBitWidth()) {
		value = m_builder->CreateTrunc(value, type);
	} else if (it->getBitWidth() < itype->getBitWidth()) {
		value = m_builder->CreateSExt(value, type);
	}
	this->m_builder->CreateStore(value, target);
}