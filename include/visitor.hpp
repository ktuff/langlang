#pragma once

#include <stack>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/Support/raw_ostream.h>

#include "LLParserBaseVisitor.h"

class ValueType {
};

class Param {
};

class Function {

	private:
		std::string const m_name;
		std::vector<Param> m_params;
		std::vector<ValueType> m_returntype;
};


class LangVisitor : public ll::LLParserBaseVisitor {

	private:
		std::unique_ptr<llvm::LLVMContext> m_context;
		std::unique_ptr<llvm::Module> m_module;
		std::unique_ptr<llvm::IRBuilder<>> m_builder;
		std::unordered_map<std::string, Function> m_functions;

		std::unordered_map<std::string, llvm::Value*> m_local_variables;
		llvm::Value* m_return_value;
		llvm::BasicBlock* m_current_bb;


	public:
		LangVisitor();
		~LangVisitor();


	public:
		antlrcpp::Any visitProgram(ll::LLParser::ProgramContext *ctx) override;
		antlrcpp::Any visitFunction(ll::LLParser::FunctionContext *ctx) override;
		antlrcpp::Any visitStatement_block(ll::LLParser::Statement_blockContext * ctx) override;
		antlrcpp::Any visitReturn_stmt(ll::LLParser::Return_stmtContext *ctx) override;
		antlrcpp::Any visitDecl_stmt(ll::LLParser::Decl_stmtContext *ctx) override;
		antlrcpp::Any visitExpr_stmt(ll::LLParser::Expr_stmtContext *ctx) override;
		antlrcpp::Any visitIf_stmt(ll::LLParser::If_stmtContext *ctx) override;
		antlrcpp::Any visitFor_stmt(ll::LLParser::For_stmtContext *ctx) override;
		antlrcpp::Any visitWhile_stmt(ll::LLParser::While_stmtContext *ctx) override;
		antlrcpp::Any visitBinary_expr(ll::LLParser::Binary_exprContext *ctx) override;
		antlrcpp::Any visitUnary_expr(ll::LLParser::Unary_exprContext *ctx) override;
		antlrcpp::Any visitValue(ll::LLParser::ValueContext *ctx) override;

		void emit_object(std::string const& output);

	private:
		llvm::Type* get_type(ll::LLParser::Type_builtinContext* builtin) const;
		llvm::Value* to_element(llvm::Value* value);
		void store_value(llvm::Value* value, llvm::Value* target);
};
